import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Demo extends JFrame {

    public static void main(String[] args) {
	Demo demo = new Demo();
	demo.setVisible(true);
    }
    
    public Demo() {
        init();
    }

    public void init() {
        Game gamePan = new Game();
        gamePan.setPreferredSize(new Dimension(gamePan.getWidth(),gamePan.getHeight()));
        add(gamePan);
        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Magic Tower");
    }

}

class Game extends JPanel{
    
    private final int OFFSET = 0;
    private final int SPACE = 32;
    private final int W = 19 * SPACE;
    private final int H = 13 * SPACE;
    
    
    Item[][] BG = new Item[19][13];
    Item[][] Sys = new Item[19][13];
    Item[][][] iArr = new Item[10][11][11];
    private int curFloor = 1;
    private boolean floorChg = false;
    private Player p;
    private int move_mode;
    private int page=1;
    private String chara = " ";
    private String dia = " ";
    private boolean gameOver = false;
    private boolean youWin = false;
    private boolean last_boss = false;   
    
    /* 
     * -: material 1 (outside), _=z: material 2 (status), 
     * (space)=y: ground, #: wall, @=x: player,  Z:empty
     * 
     * Characters:
     * A-S: 19 monsters, 
     * T: fairy, U:old man, V: merchant, [W]: altar(1*3)
     * 
     * 1-0: digits,
     * 
     * Things:
     * a: yellow key, b: red key, c: blue key,
     * d: yellow door, e: red door, f: blue door, 
     * g: red poison, h: blue poison, i: red gem, j: blue gem,
     * k: up stair, l: down stair, m: special door, n: wand
     * 
     * Icons:
     * p: lv (1*3), q: hp, r: atk, s: def, t:money,
     * u: yellow key owned, v: red key owned, w: blue key owned, Y: life-cost, X: ?, +: right =: left
     */
     private String background =
	      "-------------------\n"
            + "-k____-" + "           -\n"
            + "-q____-" + "           -\n"
            + "-r____-" + "           -\n"
            + "-s____-" + "           -\n"
            + "-t____-" + "           -\n"
            + "-------" + "           -\n"
            + "-u____-" + "           -\n"
            + "-w____-" + "           -\n"
            + "-v____-" + "           -\n"
            + "-_____-" + "           -\n"
            + "-_____-" + "           -\n"
            + "-------------------";
 	int[][] m_count = {
	{1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	{7,6,1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	{7,5,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
	{0,0,0,2,3,5,3,2,0,0,0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0},
	{0,0,0,0,0,0,0,4,0,0,4,3,0,3,0,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,0,2,2,0,6,2,0,0,0,0},
	{0,0,0,0,0,0,0,0,0,2,1,1,1,1,3,1,3,0,0},
	{0,0,0,0,0,0,0,0,0,2,2,2,2,0,2,1,2,0,0},
	{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0},
	};
	
	String[][] stringmap = {
			{
	                "    #m#    ",
			"    #a#    ",
			"   ##T##   ",
			"   #   #   ",
			"   ##d##   ",
			"           ",
			"           ",
			"    #f#    ",
			"A#  #e#  #B",
			"g#  #U#  # ",
			"c#  #k#  #b"
			},{
			"i g#gcj#g j",
			" B # # # B ",
			"#d##D#D##d#",
    		        "     B     ",
			"A######### ",
			"    B    # ",
			"#  ##B## #A",
			"gAAg#ai# #B",
			"#d###cU# #A",
			" CAg#### d ",
			"i Ac#l   #k"
			},{
                        " B #WWW# B ",  
                        " # #i j# # ",
                        " # a#f## # ",
                        " #  A A  # ",
                        " ####d#### ",
                        " B  C  Bi#A",
                        "B# #####j#A",
                        " # #igj#A#A",
                        "i# #E  # # ",
                        "j# #d### f ",
                        "a#   V#kA#l"
			},{
			"g#iFj#g# F ",
			"i# #Hf # # ",
			"j#bFh# # #G",
			" ##### G # ",
			" FE  #d###a",
			"####   hU# ",
			"  F  H   # ",
			" #######E#E",
			"   DDaa# # ",
			"d####### # ",
			"  G  k#l g "
			},{
			"ggg#ckc#hhh",
			"a  f I f  a",
			"jjj#   #iii",
			"##### #####",
			"    #e#    ",
			"    h U    ",
			"    # #    ",
			"           ",
			"### # # ###",
			"g #     # g",
			"jHd #l# fGi"
			},{
			"ihj# l #WWW",
			"a  f   f   ",
			"#d##K#H##d#",
			" Na# d #aL ",
			"   #H#K#   ",
			"#d## # ##d#",
			" Na#H#K#aL ",
			"   # d #   ",
			"#d##K#H##d#",
			" N       L ",
			"     k     "  
			},{
			"k a#i g#c h",
			"  Nd  Nd   ",
			" L # N # K ",
			"#d###d###f#",
			"a a#  a#  j",
			"   fKU dN  ",
			"aN #  a#  g",
			"#d###d###d#",
			"h  #aN # Qc",
			"  Ld   dQ  ",
			"V  # l #  h"
			},{
			"l         k",
			"#####f#####",
			"i a#Q Q# N ",
			" Q #   # # ",
			"j  f U   # ",
			"########f# ",
			"aJ O   L #K",
			"J  ##f## # ",
			"   #  h# d ",
			"O  P b # # ",
			"iO #  h# M "
			},{
			" O #      l",
			" # d #     ",
			" #L#L# #d#M",
			" # # M #   ",  
			" # K # #hhh",
			"Q##d##Q####",
			" #iaJ    #h",
			"f#######O#h",
			" h# g #U # ",
			"  dP Jd  #K",
			"k # g #g   "
			},{			
			"     n     ",
			"hhh#[[[#ggg",
			"hhh#RRR#ggg",
			"hhh#]]]#ggg",
			"jjj#   #iii",
			"jjj#   #iii",
			"jjj#   #iii",
			"#####e#####",
			"      U    ",
			"           ",
			"l          "
			}  

                };	 	

		 
	/*String[] stringmap = new String[11];
		stringmap[0] = "    #m#    ";
		stringmap[1] = "    #a#    ";
		stringmap[2] = "   ## ##   ";
		stringmap[3] = "   # @ #   ";
		stringmap[4] = "   ##d##   ";
		stringmap[5] = "           ";
		stringmap[6] = "           ";
		stringmap[7] = "    #f#    ";
		stringmap[8] = "A#  #e#  #B";
		stringmap[9] = "g#  #U#  # ";
		stringmap[10] = "c#  #k#  #b";	*/    
    
    public Game () {
	initBG();
        initWorld();
	addKeyListener(new KeyAdp());
	move_mode=0;
        setFocusable(true);
    }
    
     public final void initBG() {
	int x = 0;
        int y = 0;

        for (int i = 0; i < background.length(); i++) {

            char item = background.charAt(i);

            if (item == '\n') {
                y ++;
                x = 0;
            } 
            else if (item == '#') {
        	Wall wall = new Wall(x, y);
                BG[x][y] = (Item) wall;
                x++;
            } 
            else if (item == ' ') {
        	Ground ground = new Ground(x, y);
                BG[x][y] = (Item) ground;
                x++;
            }
            else if (item == '-') {
        	Material mat = new Material(x, y, item);
                BG[x][y] = (Item) mat;
                x++;
            }
            else if (item == '_') {
        	Material mat = new Material(x, y, 'z');
                BG[x][y] = (Item) mat;
                x++;
            }
            else {
        	Icon icon = new Icon(x, y, item);
                Sys[x][y] = (Item) icon;
        	Material mat = new Material(x, y, 'z');
                BG[x][y] = (Item) mat;
                x++;
            }
        }
	Digit digit = new Digit(4, 1, '0');
        Sys[4][1] = (Item) digit;
	digit = new Digit(5, 1, '1');
        Sys[5][1] = (Item) digit;
	
	digit = new Digit(2, 2, 'Z');
        Sys[2][2] = (Item) digit;
	digit = new Digit(3, 2, '1');
        Sys[3][2] = (Item) digit;
	digit = new Digit(4, 2, '0');
        Sys[4][2] = (Item) digit;
	digit = new Digit(5, 2, '0');
        Sys[5][2] = (Item) digit;
	
	digit = new Digit(3, 3, 'Z');
        Sys[3][3] = (Item) digit;
	digit = new Digit(4, 3, '1');
        Sys[4][3] = (Item) digit;
	digit = new Digit(5, 3, '0');
        Sys[5][3] = (Item) digit;
	
	digit = new Digit(3, 4, 'Z');
        Sys[3][4] = (Item) digit;
	digit = new Digit(4, 4, '1');
        Sys[4][4] = (Item) digit;
	digit = new Digit(5, 4, '0');
        Sys[5][4] = (Item) digit;
	
	digit = new Digit(2, 5, 'Z');
        Sys[2][5] = (Item) digit;
	digit = new Digit(3, 5, 'Z');
        Sys[3][5] = (Item) digit;
	digit = new Digit(4, 5, 'Z');
        Sys[4][5] = (Item) digit;
	digit = new Digit(5, 5, '0');
        Sys[5][5] = (Item) digit;

	digit = new Digit(4, 7, 'Z');
        Sys[4][7] = (Item) digit;
	digit = new Digit(5, 7, '0');
        Sys[5][7] = (Item) digit;
	
	digit = new Digit(4, 8, 'Z');
        Sys[4][8] = (Item) digit;
	digit = new Digit(5, 8, '0');
        Sys[5][8] = (Item) digit;	
	
	digit = new Digit(4, 9, 'Z');
        Sys[4][9] = (Item) digit;
	digit = new Digit(5, 9, '0');
        Sys[5][9] = (Item) digit;
    }
    
     public final void initWorld() {
	//Sys.out.printf("yes\n");
	for (int i = 0; i < 10; i++)
        for (int j = 0; j < 11; j++) 
	for (int k = 0; k < 11; k++) 
	{
            char item = stringmap[i][j].charAt(k);

            if (item == '#') {
        	Wall wall = new Wall(k+7, j+1);
                iArr[i][k][j] = (Item) wall;
            }
            else if (item == ' ') {
        	Ground ground = new Ground(k+7, j+1);
                iArr[i][k][j] = (Item) ground;
            }
            else if (item == 'W') { //deal with altar
        	Charac charac = new Charac(k+7, j+1, item);
                iArr[i][k][j] = (Item) charac;
                Charac charac2 = new Charac(k+8, j+1, item);
                charac2.noShow();
                iArr[i][k+1][j] = (Item) charac2;
                Charac charac3 = new Charac(k+9, j+1, item);
                charac3.noShow();
                iArr[i][k+2][j] = (Item) charac3;
                k+=2;
            }
            else if (item == '[') {
        	Charac charac = new Charac(k+7, j+1, item);
                iArr[i][k][j] = (Item) charac;
                Charac charac2 = new Charac(k+8, j+1, item);
                charac2.noShow();
                iArr[i][k+1][j] = (Item) charac2;
                Charac charac3 = new Charac(k+9, j+1, item);
                charac3.noShow();
                iArr[i][k+2][j] = (Item) charac3;
                k+=2;
            }
            else if (item == 'R') {
        	Charac charac = new Charac(k+7, j+1, item);
                iArr[i][k][j] = (Item) charac;
                Charac charac2 = new Charac(k+8, j+1, item);
                charac2.noShow();
                iArr[i][k+1][j] = (Item) charac2;
                Charac charac3 = new Charac(k+9, j+1, item);
                charac3.noShow();
                iArr[i][k+2][j] = (Item) charac3;
                k+=2;
            }
            else if (item == ']') {
        	Charac charac = new Charac(k+7, j+1, item);
                iArr[i][k][j] = (Item) charac;
                Charac charac2 = new Charac(k+8, j+1, item);
                charac2.noShow();
                iArr[i][k+1][j] = (Item) charac2;
                Charac charac3 = new Charac(k+9, j+1, item);
                charac3.noShow();
                iArr[i][k+2][j] = (Item) charac3;
                k+=2;
            }
            else if (Character.isUpperCase(item)) {
        	Charac charac = new Charac(k+7, j+1, item);
                iArr[i][k][j] = (Item) charac;
            }
            else if (item-'a' >= 0 && item-'a' <= ('n'-'a')) {
        	Thing thing = new Thing(k+7, j+1, item);
                iArr[i][k][j] = (Item) thing;
            }
        }
	p = new Player(12*SPACE,4*SPACE);
	Map[] map = new Map[10];
	for (int i=0; i<10; i++)
		map[i] = new Map(stringmap[i]);
	p.init(100 ,10,10, 5 ,3, map);
	//p.set_money(50);
    }
    
    public void buildWorld(Graphics g) {
        g.setColor(new Color(250, 240, 170));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        
        
        for (int i = 0; i < 19; i++) {
            for (int j = 0; j < 13; j++) {
        	//Item item = background[i][j];
        	g.drawImage(BG[i][j].getImg(), i*SPACE, j*SPACE, this);
		
            	if (Sys[i][j] != null) {
            	    //item = iArr[curFloor-1][i][j][1];
            	    if (Sys[i][j].exist() && Sys[i][j].show()) {
            		g.drawImage(Sys[i][j].getImg(), i*SPACE, j*SPACE, this);
            	    }
            	}
            }
        }
         for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
		//System.out.printf("%d %d %d\n",curFloor-1,i,j);
        	Item item = iArr[curFloor-1][i][j];
        	if (item.exist() && item.show()) {
        	    g.drawImage(item.getImg(), (i+7)*SPACE, (j+1)*SPACE, this);
        	}
            }
        }       
        g.drawImage(p.getImg(), p.x(), p.y(), this);
        if (gameOver) {
            Image img = Toolkit.getDefaultToolkit().getImage("pic/die.jpg");
            g.drawImage(img, 0, 0, this);
        } else if (youWin) {
            Image img = Toolkit.getDefaultToolkit().getImage("pic/win.jpg");
            g.drawImage(img, 0, 0, this);
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
	if (move_mode==0){
        buildWorld(g);
	printScr (g, chara, dia);
	}else if (move_mode==1)
	buildGuide (g, curFloor-1, page);
    }
/*
    class KeyAdp extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            p.move(SPACE, 0);
	    
            repaint();
        }
    }
  */
	class KeyAdp extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e)
		{
		  if (move_mode==0){
			if(!p.talk && p.ppl instanceof Fairy){
				Fairy f = (Fairy) p.ppl;
				if (last_boss==true){
					Monster boss = new Monster(19,"Great Magic Master",2000,250,50,0,(new Skill()),f.get_x(),f.get_y(),'S');
					iArr[curFloor-1][f.get_x()][f.get_y()] = (Item) boss;
					p.gameMap[curFloor-1].set_stringmap(f.get_x(),f.get_y(),"S");
					p.gameMap[curFloor-1].put_instance(f.get_y(),f.get_x(),(boss));
					m_count[0][18]=1;
					chara = "[Great Magic Master]:";
					dia = "Now ... Die!";
					p.ppl = null;
					repaint();
					return;
				}
				Ground ground = new Ground(f.get_x(),f.get_y());
				iArr[curFloor-1][f.get_x()][f.get_y()] = (Item) ground;
				chara = " ";
				dia = " ";
				p.ppl = null;
				repaint();
			}
			if(!p.talk && p.ppl instanceof Oldman){
				Oldman f = (Oldman) p.ppl;
				Ground ground = new Ground(f.get_x(),f.get_y());
				iArr[curFloor-1][f.get_x()][f.get_y()] = (Item) ground;
				chara = " ";
				dia = " ";
				p.ppl = null;
				repaint();
			} 
			switch(e.getKeyCode())
			{
			    case KeyEvent.VK_UP	:{
			        if(p.altar)
				{
					return;
				}
				if(p.talk){   
					return;
				}
				chara = " ";
				dia = " ";
				int move_return = p.move(p.position_x,p.position_y-1);
				if (move_return==19){
					chara = "[Great Magic Master]:";
					dia = "No! It can't be!";
					repaint();
				}
				if (move_return>=0){
					String[] status = p.get_status();
					if (p.get_floor()==curFloor){
						Ground ground = new Ground(p.position_x+7, p.position_y+1);
						iArr[curFloor-1][p.position_x][p.position_y] = (Item) ground;
					} else
						curFloor = p.get_floor();
					set_digit(status);
					if (move_return>=1)
						m_count[curFloor-1][move_return-1]-=1;
					if (move_return==18){
						Ground ground = new Ground(p.position_x+7, p.position_y+1);
						iArr[curFloor-1][4][1] = (Item) ground;
						iArr[curFloor-1][5][1] = (Item) ground;
						iArr[curFloor-1][6][1] = (Item) ground;
						iArr[curFloor-1][4][2] = (Item) ground;
						iArr[curFloor-1][5][2] = (Item) ground;
						iArr[curFloor-1][6][2] = (Item) ground;
						iArr[curFloor-1][4][3] = (Item) ground;
						iArr[curFloor-1][5][3] = (Item) ground;
						iArr[curFloor-1][6][3] = (Item) ground;
					}
					repaint();
				}
				if (move_return == -4){
				    gameOver = true;
				    repaint();
				}
					    if(p.talk && p.ppl instanceof Fairy)
					    {
						Fairy f = (Fairy) p.ppl;
						if(p.ppl instanceof Fairy)
						 {
							chara = "[Fairy]:";
							dia = f.conversation(p);
							repaint();
						 }              
						return;
					    }
					    if(p.talk && p.ppl instanceof Oldman)
					    {
						Oldman f = (Oldman) p.ppl;
							chara = "[Oldman]:";
							dia = f.conversation(p, curFloor);
							repaint();
							return;
					    }  
					if(p.altar && p.ppl instanceof Store){
						Store s = (Store)p.ppl;
						s.move_index(0);
						chara = s.talk();
						dia = s.s_index();
						repaint();
						return;
					}
					break;
			    }case KeyEvent.VK_DOWN :{
				if(p.altar)
				{
					return;
				}
				if(p.talk){         
						return;
				}
				chara = " ";
				dia = " ";				
				int move_return = p.move(p.position_x,p.position_y+1);
				if (move_return>=0){
					String[] status = p.get_status();
					if (p.get_floor()==curFloor){
						Ground ground = new Ground(p.position_x+7, p.position_y+1);
						iArr[curFloor-1][p.position_x][p.position_y] = (Item) ground;
					} else
						curFloor = p.get_floor();
					set_digit(status);
					if (move_return>=1)
						m_count[curFloor-1][move_return-1]-=1;
					repaint();
				}
				if (move_return == -4) {
			  		  //System.out.println("kill by mon 3");
				    gameOver = true;
				    repaint();
				}
					    if(p.talk && p.ppl instanceof Oldman)
					    {
						Oldman f = (Oldman) p.ppl;
							chara = "[Oldman]:";
							dia = f.conversation(p, curFloor);
							repaint();
							return;
					    }               		
				break;  
			    }case KeyEvent.VK_LEFT :{
			        if(p.talk){           
						return;
				}
				if(p.altar && p.ppl instanceof Store){
					Store s = (Store)p.ppl;
					s.move_index(s.get_index()-1);
					chara = s.talk();
					dia = s.s_index();
					repaint();
					return;
				}
				if(p.altar && p.ppl instanceof Merchant){
					Merchant s = (Merchant)p.ppl;
					s.move_index(s.get_index()-1);
					String charge = String.valueOf(s.get_charge());
					chara = s.talk(0);
					dia = s.s_index();
					repaint();
					return;
				}
				chara = " ";
				dia = " ";
				int move_return = p.move(p.position_x-1,p.position_y);
				if (move_return>=0){
					String[] status = p.get_status();
					if (p.get_floor()==curFloor ){
						Ground ground = new Ground(p.position_x+7, p.position_y+1);
						iArr[curFloor-1][p.position_x][p.position_y] = (Item) ground;
					} else
						curFloor = p.get_floor();
					set_digit(status);
					if (move_return>=1)
						m_count[curFloor-1][move_return-1]-=1;
					repaint();	
				}
				if (move_return == -4) {
			  		  System.out.println("kill by mon 3");
				    gameOver = true;
				    repaint();
				}
					    if(p.talk && p.ppl instanceof Oldman)
					    {
						Oldman f = (Oldman) p.ppl;
							chara = "[Oldman]:";
							dia = f.conversation(p, curFloor);
							repaint();
							return;
					    }  
				break;
			    }case KeyEvent.VK_RIGHT :{
				if(p.talk){              
						return;
				}
				if(p.altar && p.ppl instanceof Store){
					Store s = (Store)p.ppl;
					s.move_index(s.get_index()+1);
					chara = s.talk();
					dia = s.s_index();
					repaint();
					return;
				}
				if(p.altar && p.ppl instanceof Merchant){
					Merchant s = (Merchant)p.ppl;
					s.move_index(s.get_index()+1);
					String charge = String.valueOf(s.get_charge());
					chara = s.talk(0);
					dia = s.s_index();
					repaint();
					return;
				}
				chara = " ";
				dia = " ";
				int move_return = p.move(p.position_x+1,p.position_y);
				if (move_return>=0){
					String[] status = p.get_status();
					if (p.get_floor()==curFloor){
						Ground ground = new Ground(p.position_x+7, p.position_y+1);
						iArr[curFloor-1][p.position_x][p.position_y] = (Item) ground;
					} else
						curFloor = p.get_floor();
					set_digit(status);
					if (move_return>=1)
						m_count[curFloor-1][move_return-1]-=1;
					repaint();
				}
				if (move_return == -4) {
			  		  System.out.println("kill by mon 3");
				    gameOver = true;
				    repaint();
				}
					    if(p.talk && p.ppl instanceof Oldman)
					    {
						Oldman f = (Oldman) p.ppl;
							chara = "[Oldman]:";
							dia = f.conversation(p, curFloor);
							repaint();
							return;
					    }  
					break;
				}case KeyEvent.VK_ENTER :{
					if (gameOver || youWin) 
					    System.exit(0);
					if(p.altar && p.ppl instanceof Store)
					    {
						Store s = (Store)p.ppl;
						s.sell(p);
						chara = s.talk();
						if (s.get_index()==3){
							p.altar = false;
							chara = " ";
							dia = " ";
							//repaint();
						}
						String[] status = p.get_status();
						set_digit(status);
						repaint();
						return;
					    }
					    if(p.altar && p.ppl instanceof Merchant)
					    {
						Merchant s = (Merchant)p.ppl;
						if(s.sell(p)){
							Ground ground = new Ground(s.get_x(),s.get_y());
							iArr[curFloor-1][s.get_x()][s.get_y()] = (Item) ground;
							chara = s.talk(2);
							dia = s.talk(3);
							p.gameMap[curFloor-1].set_stringmap(s.get_x(),s.get_y()," ");
							p.gameMap[curFloor-1].put_instance(s.get_y(),s.get_x(),(new Tile(true,s.get_x(),s.get_y(),' ')));
							p.ppl = null;
							p.altar = false;
							String[] status = p.get_status();
							set_digit(status);
							repaint();
						} else {
							chara = s.talk(4);
							repaint();
						}
						if (s.get_index()==1){
							p.altar = false;
							chara = s.talk(5);
							dia = " ";
						}
						String[] status = p.get_status();
						set_digit(status);
						repaint();
						return;
					    }
					    if(p.talk && p.ppl instanceof Fairy)
					    {
						Fairy f = (Fairy) p.ppl;
						if(p.ppl instanceof Fairy)
						{
							chara = "[Fairy]:";
							dia = f.conversation(p);
							repaint();
						}
              
						return;
					    }   
					    if(!p.talk && p.ppl instanceof Oldman){
						Oldman f = (Oldman) p.ppl;
						Ground ground = new Ground(f.get_x(),f.get_y());
						iArr[curFloor-1][f.get_x()][f.get_y()] = (Item) ground;
						chara = " ";
						dia = " ";
						repaint();
						return;	
						}  
					chara = " ";
					dia = " ";
					move_mode = 1;
					repaint();
					break;
				}
				default:
			}
		  }else if (move_mode==1)
			switch(e.getKeyCode())
			{
			    case KeyEvent.VK_UP	:{
				break;
			    }case KeyEvent.VK_DOWN :{	
				break;  
			    }case KeyEvent.VK_LEFT :{
				if (page==2)
					page=1;
				repaint();
				break;
			    }case KeyEvent.VK_RIGHT :{
				if (page==1)
					page=2;
				repaint();
				break;
			    }case KeyEvent.VK_ENTER :{
					move_mode = 0;	
					page=1;
					repaint();
				break;
			    }
				default:
			}	
		if (p.get_floor()==10 && p.position_x==5 && p.position_y==0 && last_boss == false) {
		//if (p.get_floor()==10 && p.position_x==5 && p.position_y==0) {
			chara = "This must be the wand the fairy talked about.";
			dia = " ";			
			last_boss = true;
			p.last_boss = true;
			Fairy fairy = new Fairy(5,2,'T');
			iArr[0][5][2] = (Item) fairy;
			p.gameMap[0].set_stringmap(5,2,"T");
			p.gameMap[0].put_instance(2,5,(fairy));
			repaint();
		}
		if (p.get_floor()==1 && p.position_x==5 && p.position_y==1 && last_boss == true) {
			youWin=true;
		}
		}
	}
	
	
    public void restart() {

    }
    
    public int getWidth() {
        return W;
    }

    public int getHeight() {
        return H;
    }
    public void set_digit(String[] Status){
	
	char[] floor =  Status[0].toCharArray();
	char[] hp = Status[1].toCharArray();
	char[] atk = Status[2].toCharArray();
	char[] def = Status[3].toCharArray();
	char[] gold = Status[4].toCharArray();
	char[] yk = Status[5].toCharArray();
	char[] bk = Status[6].toCharArray();
	char[] rk = Status[7].toCharArray();
        Sys[4][1].setImg(floor[2]);
        Sys[5][1].setImg(floor[3]);
	
        if(hp[0]!='0')Sys[2][2].setImg(hp[0]);
	else Sys[2][2].setImg('Z');
	if(hp[0]=='0'&& hp[1]=='0') Sys[3][2].setImg('Z');
	else Sys[3][2].setImg(hp[1]);
	if(hp[0]=='0'&& hp[1]=='0' && hp[2]=='0') Sys[4][2].setImg('Z');
	else Sys[4][2].setImg(hp[2]);
        Sys[5][2].setImg(hp[3]);

        if(atk[1]!='0')Sys[3][3].setImg(atk[1]);
	else Sys[3][3].setImg('Z');
        Sys[4][3].setImg(atk[2]);
        Sys[5][3].setImg(atk[3]);
	
        if(def[1]!='0')Sys[3][4].setImg(def[1]);
	else Sys[3][4].setImg('Z');
        Sys[4][4].setImg(def[2]);
        Sys[5][4].setImg(def[3]);
	
        if(gold[0]!='0')Sys[2][5].setImg(gold[0]);
	else Sys[2][5].setImg('Z');
        if(gold[0]=='0'&& gold[1]=='0')Sys[3][5].setImg('Z');
	else Sys[3][5].setImg(gold[1]);
	if(gold[0]=='0'&& gold[1]=='0'&& gold[2]=='0')Sys[4][5].setImg('Z');
        else Sys[4][5].setImg(gold[2]);
        Sys[5][5].setImg(gold[3]);

        if(yk[2]!='0')Sys[4][7].setImg(yk[2]);
	else Sys[4][7].setImg('Z');
        Sys[5][7].setImg(yk[3]);
	
        if(bk[2]!='0')Sys[4][8].setImg(bk[2]);
	else Sys[4][8].setImg('Z');
        Sys[5][8].setImg(bk[3]);	
	
        if(rk[2]!='0')Sys[4][9].setImg(rk[2]);
	else Sys[4][9].setImg('Z');
        Sys[5][9].setImg(rk[3]);
    }
    
    private void printScr (Graphics g, String chara, String dia) {
	g.setColor(Color.white);
	g.setFont(new Font("monospaced", Font.BOLD , 14));
	if (chara== "[Fairy]:" )
	g.drawString( chara + "      (press enter)", (int) (SPACE * 0.5) , (int) (SPACE * 12) + 15);
	else
	g.drawString( chara , (int) (SPACE * 0.5) , (int) (SPACE * 12) + 15);	
	g.setFont(new Font("monospaced", Font.BOLD|Font.ITALIC , 14));
	g.drawString(dia, (int) (SPACE * 0.5) , SPACE * 13 - 3);
	chara = " ";
	dia = " ";
    }
     
    private void buildGuide (Graphics g, int floor, int page){
	g.setColor(new Color(250, 240, 170));
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        
        
        for (int i = 0; i < 19; i++) {
            for (int j = 0; j < 13; j++) {
        	//Item item = background[i][j];
        	g.drawImage(BG[i][j].getImg(), i*SPACE, j*SPACE, this);
		
            	if (Sys[i][j] != null) {
            	    //item = iArr[curFloor-1][i][j][1];
            	    if (Sys[i][j].exist()) {
            		g.drawImage(Sys[i][j].getImg(), i*SPACE, j*SPACE, this);
            	    }
            	}
            }
        }
	int[] mons = new int[8];
	int count=0;
	for (int i=0; i<19 && count<8;i++)
		if (m_count[floor][i]>0){
			mons[count]=i+1;
			count++;
			//System.out.printf("floor=%d i=%d m_count[floor][i]=%d mons=%d count=%d\n",floor, i, m_count[floor][i],mons[count-1],count-1);
		}
	for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
		//System.out.printf("%d %d %d\n",curFloor-1,i,j);
        	//Item item = iArr[curFloor-1][i][j];
        	//if (item.exist()) {
        	    g.drawImage(BG[1][1].getImg(), (i+7)*SPACE, (j+1)*SPACE, this);
        	//}
            }
	}
	if (page==2 && count <= 4)
		page=1;
	if (page==1 && count>4){
		Icon icon = new Icon((18)*SPACE, 6*SPACE, '+');
		g.drawImage(icon.getImg(), (18)*SPACE+4, (6)*SPACE, this);
	}else if (page==2){
		Icon icon = new Icon((8)*SPACE+10, 6*SPACE, '=');
		g.drawImage(icon.getImg(), (6)*SPACE+12, 6*SPACE, this);
	}
	if (count>0 && page==1 || count>4 && page==2)
	for (int i = 0; i< 4; i++)
		if (mons[(page-1)*4+i]!=0){
			char a = (char)(mons[(page-1)*4+i]+64);
			//System.out.printf("%d %d %c\n",mons[i],i,a);
			Thing thing = new Thing((8)*SPACE, (i*2+2)*SPACE+i*16, a);
			g.drawImage(thing.getImg(), (8)*SPACE-16, (i*2+2)*SPACE+i*16, this);
			if (a=='R'){
				thing = new Thing((8)*SPACE-16, (i*2+2)*SPACE+i*16-32, '[');
				g.drawImage(thing.getImg(), (8)*SPACE-16, (i*2+2)*SPACE+i*16-32	, this);
				thing = new Thing((8)*SPACE-16, (i*2+2)*SPACE+i*16, ']');
				g.drawImage(thing.getImg(), (8)*SPACE-16, (i*2+2)*SPACE+i*16+32, this);
			}
			Icon icon = new Icon((8)*SPACE, (i*2+2)*SPACE+i*16, 'q');
			g.drawImage(icon.getImg(), (9)*SPACE, (i*2+2)*SPACE+i*16-18, this);
			icon = new Icon((8)*SPACE, (i*2+2)*SPACE+i*16, 'r');
			g.drawImage(icon.getImg(), (9)*SPACE, (i*2+2)*SPACE+i*16+18, this);
			icon = new Icon((8)*SPACE, (i*2+2)*SPACE+i*16, 's');
			g.drawImage(icon.getImg(), (12)*SPACE, (i*2+2)*SPACE+i*16+18, this);
			icon = new Icon((8)*SPACE, (i*2+2)*SPACE+i*16, 'Y');
			g.drawImage(icon.getImg(), (13)*SPACE, (i*2+2)*SPACE+i*16-18, this);
			icon = new Icon((8)*SPACE, (i*2+2)*SPACE+i*16, 't');
			g.drawImage(icon.getImg(), (15)*SPACE, (i*2+2)*SPACE+i*16+18, this);
			Monster mon;
			int hp=0,atk=0,def=0,gold=0,damage=0;
			int gold_before = p.get_money();
			int health_before = p.get_health();
			
				switch(a){
					case 'A':
						mon = new Monster(1,"Green Slime",20,20,3,1,(new Skill()),0,0,'A');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'B':
						mon = new Monster(2,"Red Slime",30,25,5,2,(new Skill()),0,0,'B');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'C':
						mon = new Monster(3,"Little Bat",50,30,4,4,(new Lifesteal()),0,0,'C');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'D':
						mon = new Monster(4,"Blue Bat",80,40,9,12,(new Lifesteal()),0,0,'D');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'E':
						mon = new Monster(5,"Black Slime",150,35,10,7,(new Skill()),0,0,'E');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'F':
						mon = new Monster(6,"Blue priest",100,50,0,20,(new Magic_Attack()),0,0,'F');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						
						break;
					case 'G':
						mon = new Monster(7,"Skeleton",175,55,15,33,(new Skill()),0,0,'G');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'H':
						mon = new Monster(8,"Zombie",160,60,20,54,(new Undead(5)),0,0,'H');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'I':
						mon = new Monster(9,"Assassin",150,75,25,100,(new Rush()),0,0,'I');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'J':	
						mon = new Monster(10,"Red Bat",200,80,45,120,(new Lifesteal()),0,0,'J');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'K':
						mon = new Monster(11,"Skeleton soldier",250,90,40,55,(new Skill()),0,0,'K');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'L':
						mon = new Monster(12,"Zombie soldier",220,75,45,60,(new Undead(10)),0,0,'L');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'M':
						mon = new Monster(13,"Rock",20,70,70,96,(new Skill()),0,0,'M');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'N':
						mon = new Monster(14,"Red priest",200,75,5,40,(new Magic_Attack()),0,0,'N');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'O':
						mon = new Monster(15,"Gate Keeper",500,65,55,135,(new Skill()),0,0,'O');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'P':
						mon = new Monster(16,"Knight",600,100,40,150,(new Skill()),0,0,'P');	
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'Q':
						mon = new Monster(17,"Magician",300,100,10,80,(new Magic_Attack()),0,0,'Q');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
					case 'R':
						mon = new Monster(18,"Dragon",600,150,60,760,(new Skill()),0,0,'R');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);	
						break;
					case 'S':
						mon = new Monster(19,"Great Magic Master",2000,250,50,0,(new Skill()),0,0,'S');
						hp = mon.get_health();
						atk = mon.get_attack();
						def = mon.get_defense();
						gold = mon.get_money();
						if(!p.fight(mon))
							damage = 10000;
						else
							damage = health_before - p.get_health();
						p.set_health(health_before);
						p.set_money(gold_before);
						break;
				}
				Digit digit;
				//System.out.printf("i=%d damage=%d",i, damage);
				if (hp>=1000) {
					digit = new Digit((9)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(hp/1000+48));
					g.drawImage(digit.getImg(), (9)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
					hp%=1000;
					digit = new Digit((9)*SPACE+47, (i*2+2)*SPACE+i*16-18, (char)(hp/100+48));
					g.drawImage(digit.getImg(), (9)*SPACE+47, (i*2+2)*SPACE+i*16-18, this);
					hp%=100;
					digit = new Digit((9)*SPACE+66, (i*2+2)*SPACE+i*16-18, (char)(hp/10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+66, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((9)*SPACE+85, (i*2+2)*SPACE+i*16-18, (char)(hp%10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+85, (i*2+2)*SPACE+i*16-18, this);
				} else if (hp>=100){
					digit = new Digit((9)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(hp/100+48));
					g.drawImage(digit.getImg(), (9)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
					hp%=100;
					digit = new Digit((9)*SPACE+47, (i*2+2)*SPACE+i*16-18, (char)(hp/10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+47, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((9)*SPACE+66, (i*2+2)*SPACE+i*16-18, (char)(hp%10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+66, (i*2+2)*SPACE+i*16-18, this);
				} else if (hp>=10){
					digit = new Digit((9)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(hp/10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((9)*SPACE+47, (i*2+2)*SPACE+i*16-18, (char)(hp%10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+47, (i*2+2)*SPACE+i*16-18, this);			
				} else {
					digit = new Digit((9)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(hp+48));
					g.drawImage(digit.getImg(), (9)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);					
				}
				
				if (atk>=100){
					digit = new Digit((9)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(atk/100+48));
					g.drawImage(digit.getImg(), (9)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);
					atk%=100;
					digit = new Digit((9)*SPACE+47, (i*2+2)*SPACE+i*16+18, (char)(atk/10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+47, (i*2+2)*SPACE+i*16+18, this);
					digit = new Digit((9)*SPACE+66, (i*2+2)*SPACE+i*16+18, (char)(atk%10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+66, (i*2+2)*SPACE+i*16+18, this);
				} else {
					digit = new Digit((9)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(atk/10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);
					digit = new Digit((9)*SPACE+47, (i*2+2)*SPACE+i*16+18, (char)(atk%10+48));
					g.drawImage(digit.getImg(), (9)*SPACE+47, (i*2+2)*SPACE+i*16+18, this);			
				}
				
				if (def>=100){
					digit = new Digit((12)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(def/100+48));
					g.drawImage(digit.getImg(), (12)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);
					def%=100;
					digit = new Digit((12)*SPACE+47, (i*2+2)*SPACE+i*16+18, (char)(def/10+48));
					g.drawImage(digit.getImg(), (12)*SPACE+47, (i*2+2)*SPACE+i*16+18, this);
					digit = new Digit((12)*SPACE+66, (i*2+2)*SPACE+i*16+18, (char)(def%10+48));
					g.drawImage(digit.getImg(), (12)*SPACE+66, (i*2+2)*SPACE+i*16+18, this);
				} else if (def>=10){
					digit = new Digit((12)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(def/10+48));
					g.drawImage(digit.getImg(), (12)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);
					digit = new Digit((12)*SPACE+47, (i*2+2)*SPACE+i*16+18, (char)(def%10+48));
					g.drawImage(digit.getImg(), (12)*SPACE+47, (i*2+2)*SPACE+i*16+18, this);				
				} else {
					digit = new Digit((12)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(def+48));
					g.drawImage(digit.getImg(), (12)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);				
				}
										
				if (gold >= 100){
					digit = new Digit((15)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(gold/100+48));
					g.drawImage(digit.getImg(), (15)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);
					gold%=100;
					digit = new Digit((15)*SPACE+47, (i*2+2)*SPACE+i*16+18, (char)(gold/10+48));
					g.drawImage(digit.getImg(), (15)*SPACE+47, (i*2+2)*SPACE+i*16+18, this);
					digit = new Digit((15)*SPACE+66, (i*2+2)*SPACE+i*16+18, (char)(gold%10+48));
					g.drawImage(digit.getImg(), (15)*SPACE+66, (i*2+2)*SPACE+i*16+18, this);
				} else if (gold >= 10){
					digit = new Digit((15)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(gold/10+48));
					g.drawImage(digit.getImg(), (15)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);
					digit = new Digit((15)*SPACE+47, (i*2+2)*SPACE+i*16+18, (char)(gold%10+48));
					g.drawImage(digit.getImg(), (15)*SPACE+47, (i*2+2)*SPACE+i*16+18, this);				
				} else {
					digit = new Digit((15)*SPACE+28, (i*2+2)*SPACE+i*16+18, (char)(gold+48));
					g.drawImage(digit.getImg(), (15)*SPACE+28, (i*2+2)*SPACE+i*16+18, this);				
				}
				
				if (damage >= 10000){				
					digit = new Digit((13)*SPACE+28, (i*2+2)*SPACE+i*16-18, 'X');
					g.drawImage(digit.getImg(), (13)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((13)*SPACE+47, (i*2+2)*SPACE+i*16-18, 'X');
					g.drawImage(digit.getImg(), (13)*SPACE+47, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((13)*SPACE+66, (i*2+2)*SPACE+i*16-18, 'X');
					g.drawImage(digit.getImg(), (13)*SPACE+66, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((13)*SPACE+85, (i*2+2)*SPACE+i*16-18, 'X');
					g.drawImage(digit.getImg(), (13)*SPACE+85, (i*2+2)*SPACE+i*16-18, this);
				} else if (damage >= 1000){	
					digit = new Digit((13)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(damage/1000+48));
					g.drawImage(digit.getImg(), (13)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
					damage%=1000;
					digit = new Digit((13)*SPACE+47, (i*2+2)*SPACE+i*16-18, (char)(damage/100+48));
					g.drawImage(digit.getImg(), (13)*SPACE+47, (i*2+2)*SPACE+i*16-18, this);
					damage%=100;
					digit = new Digit((13)*SPACE+66, (i*2+2)*SPACE+i*16-18, (char)(damage/10+48));
					g.drawImage(digit.getImg(), (13)*SPACE+66, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((13)*SPACE+85, (i*2+2)*SPACE+i*16-18, (char)(damage%10+48));
					g.drawImage(digit.getImg(), (13)*SPACE+85, (i*2+2)*SPACE+i*16-18, this);
				} else if (damage >= 100){
					digit = new Digit((13)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(damage/100+48));
					g.drawImage(digit.getImg(), (13)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
					damage%=100;
					digit = new Digit((13)*SPACE+47, (i*2+2)*SPACE+i*16-18, (char)(damage/10+48));
					g.drawImage(digit.getImg(), (13)*SPACE+47, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((13)*SPACE+66, (i*2+2)*SPACE+i*16-18, (char)(damage%10+48));
					g.drawImage(digit.getImg(), (13)*SPACE+66, (i*2+2)*SPACE+i*16-18, this);
				} else if (damage >= 10){
					digit = new Digit((13)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(damage/10+48));
					g.drawImage(digit.getImg(), (13)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
					digit = new Digit((13)*SPACE+47, (i*2+2)*SPACE+i*16-18, (char)(damage%10+48));
					g.drawImage(digit.getImg(), (13)*SPACE+47, (i*2+2)*SPACE+i*16-18, this);
				} else {
					digit = new Digit((13)*SPACE+28, (i*2+2)*SPACE+i*16-18, (char)(damage+48));
					g.drawImage(digit.getImg(), (13)*SPACE+28, (i*2+2)*SPACE+i*16-18, this);
				}
				
		}
	/*
         for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 11; j++) {
		//System.out.printf("%d %d %d\n",curFloor-1,i,j);
        	Item item = iArr[curFloor-1][i][j];
        	if (item.exist()) {
        	    g.drawImage(item.getImg(), (i+7)*SPACE, (j+1)*SPACE, this);
        	}
            }
        }*/
    }
}