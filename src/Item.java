import java.awt.Image;
import java.awt.Toolkit;


class Item {

    private int x;
    private int y;
    private Image image;
    private boolean exist;
    private boolean show;

    public Item(int x, int y, char c) {
        this.x = x;
        this.y = y;
        exist = true;
        show = true;
        if (Character.isUpperCase(c)) {
            image = Toolkit.getDefaultToolkit().getImage("pic/" + c + c + ".png");
        }
        else {
            image = Toolkit.getDefaultToolkit().getImage("pic/" + c + ".png");
        }
    }
    
    public Image getImg() {
        return this.image;
    }
    
    protected void setImg(char c) {
	image = Toolkit.getDefaultToolkit().getImage("pic/" + c + ".png");
    }

    public int x () {
        return this.x;
    }

    public int y () {
        return this.y;
    }
    
    public boolean exist () {
        return this.exist;
    }
    
    public boolean show () {
        return this.show;
    }

    protected void setX (int x) {
        this.x = x;
    }

    protected void setY (int y) {
        this.y = y;
    }
    
    protected void use () {
        exist = false;
    }

    public void noShow () {
        show = false;
    }
}