import	java.lang.*;

class	Map
{	
	private	Object[][] instances = new Object[11][11];
	private	String[] stringmap = new String[11];

	Map(String[] input_map)
	{
		for(int i =0;i<11;i++)
			stringmap[i] = input_map[i];
		
		construct_instance();

	}

	//public	int [] get_start(){return new int []{start_x,start_y};}
	public	void put_instance(int x,int y,Object new_instances){instances[x][y] = new_instances;}
	public 	void set_stringmap(int i,int j,String c)
	{
		String str;
		if(i==0)
		str = c + stringmap[j].substring(i+1);
		else if(i == stringmap[j].length()-1)
		str = stringmap[j].substring(0,i) + c;
		else
		str = stringmap[j].substring(0,i) + c + stringmap[j].substring(i+1);

		stringmap[j] = str;
		if (c=="@"){
		System.out.println("---------------");
		for(int k =0;k<11;k++){
			System.out.print("|");
			System.out.print(stringmap[k]);
			System.out.println("|");
		}
		System.out.println("---------------");
		}
	}
	public	Object get_instance(int x,int y){return instances[y][x];}
	private	void construct_instance()
	{
		int i,j;

		for(i=0;i<11;i++)
			for(j=0;j<11;j++)
			{
				switch(stringmap[i].charAt(j))
				{
					case 'a':
						put_instance(i,j,(new Key(2,j,i,'a')));
						break;
					case 'b':
						put_instance(i,j,(new Key(1,j,i,'b')));
						break;
					case 'c':
						put_instance(i,j,(new Key(0,j,i,'c')));
						break;
					case 'd':
						put_instance(i,j,(new Door(2,j,i,'d')));
						break;
					case 'e':
						put_instance(i,j,(new Door(1,j,i,'e')));
						break;
					case 'f':
						put_instance(i,j,(new Door(0,j,i,'f')));
						break;
					case 'g':
						put_instance(i,j,(new Buff(0,100,j,i,'g')));
						break;
					case 'h':
						put_instance(i,j,(new Buff(0,400,j,i,'h')));
						break;
					case 'i':
						put_instance(i,j,(new Buff(1,3,j,i,'i')));
						break;
					case 'j':
						put_instance(i,j,(new Buff(2,3,j,i,'j')));
						break;
					case 'k':
						put_instance(i,j,(new Stair(true,j,i,'k')));
						break;
					case 'l':
						put_instance(i,j,(new Stair(false,j,i,'l')));
						break;
					case 'm':
						put_instance(i,j,(new Door(3,j,i,'m')));
						break;
					case 'A':
						put_instance(i,j,(new Monster(1,"Green Slime",20,20,3,1,(new Skill()),j,i,'A')));
						break;
					case 'B':
						put_instance(i,j,(new Monster(2,"Red Slime",30,25,5,2,(new Skill()),j,i,'B')));
						break;
					case 'C':
						put_instance(i,j,(new Monster(3,"Little Bat",50,30,4,4,(new Lifesteal()),j,i,'C')));
						break;
					case 'D':
						put_instance(i,j,(new Monster(4,"Blue Bat",80,40,9,12,(new Lifesteal()),j,i,'D')));
						break;
					case 'E':
						put_instance(i,j,(new Monster(5,"Black Slime",150,35,10,7,(new Skill()),j,i,'E')));
						break;
					case 'F':
						put_instance(i,j,(new Monster(6,"Blue priest",100,50,0,20,(new Magic_Attack()),j,i,'F')));
						break;
					case 'G':
						put_instance(i,j,(new Monster(7,"Skeleton",175,55,15,33,(new Skill()),j,i,'G')));
						break;
					case 'H':
						put_instance(i,j,(new Monster(8,"Zombie",160,60,20,54,(new Undead(5)),j,i,'H')));
						break;
					case 'I':
						put_instance(i,j,(new Monster(9,"Assassin",150,75,25,100,(new Rush()),j,i,'I')));
						break;
					case 'J':	
						put_instance(i,j,(new Monster(10,"Red Bat",200,80,45,120,(new Lifesteal()),j,i,'J')));
						break;
					case 'K':
						put_instance(i,j,(new Monster(11,"Skeleton soldier",250,90,40,55,(new Skill()),j,i,'K')));
						break;
					case 'L':
						put_instance(i,j,(new Monster(12,"Zombie soldier",220,75,45,60,(new Undead(10)),j,i,'L')));
						break;
					case 'M':
						put_instance(i,j,(new Monster(13,"Rock",20,70,70,96,(new Skill()),j,i,'M')));
						break;
					case 'N':
						put_instance(i,j,(new Monster(14,"Red priest",200,75,5,40,(new Magic_Attack()),j,i,'N')));
						break;
					case 'O':
						put_instance(i,j,new Monster(15,"Gate Keeper",500,65,55,135,(new Skill()),j,i,'O'));
						break;
					case 'P':
						put_instance(i,j,(new Monster(16,"Knight",600,100,40,150,(new Skill()),j,i,'P')));
						break;
					case 'Q':
						put_instance(i,j,(new Monster(17,"Magician",300,100,10,80,(new Magic_Attack()),j,i,'Q')));
						break;
					case ']':
						put_instance(i,j,(new Monster(18,"Dragon",600,150,60,760,(new Skill()),j,i,'R')));	
						break;
					case 'S':
						put_instance(i,j,(new Monster(19,"Great Magic Master",2000,250,50,0,(new Skill()),j,i,'S')));
						break;
					case 'T':
						put_instance(i,j,(new Fairy(j,i,'T')));
						break;
					case 'U':
						put_instance(i,j,(new Oldman(j,i,'U')));
						break;
					case 'V':
						put_instance(i,j,(new Merchant(j,i,'V')));	
						break;
					case 'W':
						put_instance(i,j,(new Store(j,i,'W')));	
						break;
					case '#':
						put_instance(i,j,(new Tile(false,j,i,'#')));
						break;
					default:
						put_instance(i,j,(new Tile(true,j,i,' ')));	

				}		
			}	
	}
}