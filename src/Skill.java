import java.lang.*;

class	Skill
{
	public	void act(Monster monster,Player user)
	{
		fight(monster,user);
	}
	public	void fight(Monster monster,Player user)
	{
		int damage = monster.get_attack() - user.get_defense();
		if (damage<0)
			damage=0;
		user.set_health(user.get_health()-damage);
	}
}

class	Lifesteal extends Skill
{
	public void act(Monster monster,Player user)
	{
		fight(monster,user);
		int hp_regen = monster.get_attack() - user.get_defense();
		if (hp_regen<0)
			hp_regen=0;
		monster.set_health(monster.get_health()+hp_regen);
	}
}


class	Magic_Attack extends Skill
{
	public	void act(Monster monster,Player user)
	{
		user.set_health(user.get_health()-monster.get_attack());
	}
}	


class	Undead	extends	Skill
{
	private	int health_regen;
	Undead(int recover_ability)
	{
		health_regen = recover_ability;
	}
	public void act(Monster monster ,Player user)
	{
		fight(monster,user);
		monster.set_health(monster.get_health()+health_regen);
	}
}

class	Rush	extends	Skill
{
	public	void  act(Monster monster ,Player user)
	{
		int i;
		for(i=0;i<2;i++)
			fight(monster,user);	
	}
}