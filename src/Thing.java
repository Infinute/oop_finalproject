
public class Thing extends Item {
    
    /*
    * a: yellow key, b: red key, c: blue key,
    * d: yellow door, e: red door, f: blue door, 
    * g: red poison, h: blue poison, i: red gem, j: blue gem,
    * k: up stair, l: down stair, m: special door
    */
    public Thing (int x, int y, char c) {
        super(x, y, c);
    }

    public void use () {
        super.use();
    }
}
