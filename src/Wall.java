import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;


public class Wall extends Item {
    
    private Image image;

    public Wall (int x, int y) {
        super(x, y, '#');
    }
}
