class Fairy extends Item
{
	private String[] message;
	private int next_message;
	private int xx,yy;
	public	int get_line(){return next_message;} 
	Fairy(int x, int y, char c)
	{
		super(x,y,c);
		xx =x;
		yy =y;
		message = new String[8];
		message[0] = "Oh,You're awake.";
		message[1] = "You're trapped in this tower, and behind me is the exit locked by magic.";
		message[2] = "But if you can bring me wand, I could dispel the magic and re-open the exit.";
		message[3] = "Those monster take my wand and I can feel it's on the 10th floor.";
		message[4] = "Good luck and take care. I'll be here since you take my wand back.";
		message[5] = "Take the monster guide, you can read it by pressing enter.";
		message[6] = "My wand ... there it is (take the wand).";
		message[7] = "Finally ... I take my power back.";
	}
	public String conversation(Player user)
	{
		//System.out.printf("%d \n", next_message);
		if (user.last_boss==false){
			String return_message = message[next_message];
			if(next_message<message.length-1)
				next_message++;
			else 
				next_message = 0;

			if(next_message == 6 || next_message == 0)
				user.talk = false;
			return return_message;
		} else {
			if (next_message<6)
				next_message=6;
			String return_message = message[next_message];
			if(next_message<message.length-1)
				next_message++;
			else 
				next_message = 0;

			if(next_message == 6 || next_message == 0)
				user.talk = false;
			return return_message;		
		}
	}
	public int get_x(){return xx;}
	public int get_y(){return yy;}
}


class Oldman extends Item
{
	private int xx,yy;
	Oldman(int x, int y, char c){
		super(x,y,c);	
		xx =x;
		yy =y;
	}
	public String conversation(Player user, int floor)
	{
		user.talk = false;
		if(floor==1)	
			return "Some people cannot see the wood for the trees.";
		if(floor==2)	
			return "History repeats itself.";
		if(floor==4)	
			return "Experience teaches.";
		if(floor==5)	
			return "Saving is getting.";
		if(floor==7)	
			return "Longest way round is the shortest way home.";			
		if(floor==8)	
			return "It is never too late to mend.";
		if(floor==9)	
			return "Early mistakes are the seeds of future trouble.";
		if(floor==10)	
			return "Don't whistle until you're out of the wood.";	
		return " ";
	}
	public int get_x(){return xx;}
	public int get_y(){return yy;}
}
