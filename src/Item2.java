import java.lang.*;

class	Buff extends Item
{
	private	int type; // 
	private	int value;
	Buff(int new_type , int new_value, int x, int y, char c)
	{
		super(x,y,c);
		type = new_type; //0 = health , 1 = atk , 2 = def
		value = new_value;
	}
	public int get_value(){return value;}
	public int get_type(){return type;}
}

class	Stair extends Item
{
	private boolean direction;
	Stair(boolean new_direction, int x, int y, char c)
	{
		super(x,y,c);
	 	direction = new_direction;
	 } //true = upfloor , false = downfloor
	
	public boolean get_direction(){return direction;}
	
}

class	Key extends Item
{
	private int color;	// 0 = red , 1 = blue , 2 = yellow 
	Key(int new_color, int x, int y, char c){
		super(x,y,c);
		color = new_color;
	}
	public int getColor(){return color;}
}

class	Door extends Item
{
	private int color;	// 0 = red , 1 = blue , 2 = yellow
	Door(int new_color, int x, int y, char c){
		super(x,y,c);
		color = new_color;
	}
	public 	int getColor(){return color;}
}
class Tile extends Item
{
private boolean enable;
	Tile(boolean r, int x, int y, char c){
		super(x,y,c);
		enable = r;
	}
	public boolean get_enable(){return enable;}
}
