import java.lang.*;


class	Store extends Item
{
	static int charge = 20;
	static int time = 1;
	private int index;
	private int health;
	private int atk;
	private int def;
	Store(int x, int y, char c)
	{
		super(x,y,c);
	}	
	public	void open(int mode)
	{
		if(mode ==0){
			health=400;
			atk=2;
			def=4;
		}else{
			health=800;
			atk=4;
			def=8;		
		}
	}

	public void sell(Player user)
	{
		if(index<3 && user.get_money()>=charge)
		{
			user.set_money(user.get_money()-charge);
			charge	= charge + time*20;
			time++;
			if(index ==0)
				user.set_health(user.get_health()+health);
			else if(index == 1)
				user.set_attack(user.get_attack()+atk);
			else if(index==2)
				user.set_defense(user.get_defense()+def);
			else if(index==3)
				user.altar=false;
		}	
	}
	public void move_index(int new_index)
	{
		if(new_index<0||new_index>3)
			return;
		index = new_index;
		//System.out.println("index : " +index);
	}
	public int get_index(){return index;}
	public int get_charge(){return charge;}
	public String talk(){
		//String Charge = String.valueOf(charge);
		return "[Altar]: I can improve your skill for " + String.valueOf(charge) +" golds ";
	}
	public String s_index(){
		if (index==0)
			return "> +"+String.valueOf(health)+"hp |  +"+String.valueOf(atk)+"atk |  +"+String.valueOf(def)+"def |  leave";
		if (index==1)
			return "  +"+String.valueOf(health)+"hp |> +"+String.valueOf(atk)+"atk |  +"+String.valueOf(def)+"def |  leave";
		if (index==2)
			return "  +"+String.valueOf(health)+"hp |  +"+String.valueOf(atk)+"atk |> +"+String.valueOf(def)+"def |  leave";
		if (index==3)
			return "  +"+String.valueOf(health)+"hp |  +"+String.valueOf(atk)+"atk |  +"+String.valueOf(def)+"def |> leave";
		return " ";
	}
}

class Merchant extends Item
{
	private String[] message;
	private int charge;
	private int index;
	private int xx,yy;
	Merchant(int x,int y,char c)
	{
		super(x,y,c);

		xx = x;
		yy = y;
	}
	public	void open(int mode)
	{
		message = new String[6];
		if (mode==0){
		charge=50;
		message[0] = "Wanna buy a yellow Key for 50 golds?";
		message[1] = "My pleasure.";
		message[2] = "Be careful those monsters, some of them have special abilities.";
		message[3] = "Bats drain HP, priests' attack ignore your defense, and zombies recover hp each round.";
		message[4] = "Oops! You don't have enough golds!";
		message[5] = "Come on! You need this, huh?";
		}if (mode==1){
		charge=300;
		message[0] = "Hey! I have a blue key�K�K you want it? �Khow about�K 300 golds!";
		message[1] = "Good to be trade with you.";
		message[2] = "The priests and magicians deal a lot of damage, you need more hp to pass through.";
		message[3] = "I heard that there're some potions on 9th floor.";
		message[4] = "Bah, there aren't 300 golds.";
		message[5] = "�KYou'll regret for this.";
		}
		
	}
	public String conversation(){return message[0];}

	public boolean sell(Player user)
	{
		if(index == 0)
		{
			if(user.get_money()>=charge)
			{
				user.set_money(user.get_money()-charge);
				if (charge==50)
					user.add_key(new Key(2,0,0,'a'));
				else if(charge==300)
					user.add_key(new Key(1,0,0,'c'));
				return true;
			}
			else 
				return false;
		}

		return false;
	}
	public void move_index(int new_index)
	{
		if(new_index>1 || new_index<0)
			return;
		index = new_index;
	}
	public int get_index(){return index;}
	public int get_x(){return xx;}
	public int get_y(){return yy;}
	public int get_charge(){return charge;}
	public String talk(int mode){
		//String Charge = String.valueOf(charge);
		return message[mode];
	}
	public String s_index(){
		if (index==0)
			return "> Yes |  No";
		if (index==1)
			return "  Yes |> No";
		return " ";
	}
	
}
