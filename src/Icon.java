
public class Icon extends Item {
    
    /*
     * p: lv (1*3), q: hp, r: atk, s: def, t:money,
     * u: yellow key owned, v: red key owned, w: blue key owned
    */
    public Icon (int x, int y, char c) {
        super(x, y, c);
    }
}
