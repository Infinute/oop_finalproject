import java.lang.*;

class	Monster extends Item
{
	private int num;
	private	String name;
	private int health;
    private int attack;
  	private	int	defense;
    private int money;
	private Skill myskill; 	

 	public	int get_health(){return health;}
 	public	int get_attack(){return attack;}
 	public	int get_defense(){return defense;}
 	public	int get_money(){return money;}
 	public	int get_num(){return num;}
 	public	Skill get_skill(){return myskill;}
 	public  void set_health(int hp){health = hp;}
 	Monster(int num,String new_name,int new_health ,int new_attack,int new_defense ,int new_money ,Skill new_skill, int x, int y, char c){
		super(x,y,c);
		this.num = num;
  		this.name = new_name;
  		this.health = new_health;
  		this.attack = new_attack;
  		this.defense = new_defense;
  		this.money = new_money;
  		this.myskill = new_skill;
	}

	public	void  fight(Player user)
	{
		myskill.act(this,user);
	}
}