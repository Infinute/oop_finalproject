import	java.awt.event.*;
import	java.lang.*;
import java.awt.Image;

class Player extends Item
{
	 
	private int health;
	public int	position_x,position_y;
	private int attack;
  	private	int	defense;
	private int money=0;
	private int floor;
  	private	int	red_key_count;
  	private	int	blue_key_count;
  	private	int	yellow_key_count;
  	Map	[]gameMap;
	private int phase; 
	private Image image;
	private int floor_change; //(1 = upstair, -1 = downstair)
	public  boolean altar;//1 = at altar
	public Object ppl;
	public boolean talk;
	public boolean last_boss=false;
	
	public Player(int x, int y) {
		super(x, y, 'x');
	}

 	public void init(int new_health ,int new_attack,int new_defense ,int x ,int y, Map[] new_Map)
  	{
  		this.health = new_health;
  		this.attack = new_attack;
  		this.defense = new_defense;
		this.floor = 1;
  		this.position_x = x;
  		this.position_y = y;
  		gameMap = new Map[new_Map.length];
  		for(int i=0;i<new_Map.length;i++)
  			gameMap[i] = new_Map[i];
		

		//this.addKeyListener();
	}

  	public	void set_health(int new_health){ this.health = new_health;}
  	public	void set_attack(int new_attack){this.attack = new_attack;}
  	public  void set_defense(int new_defense){this.defense = new_defense;}		  
  	public  void set_money(int new_money){this.money = new_money;}
  	public 	int get_floor(){return floor;}  	
  	public	int get_defense(){return defense;}  	
  	public 	int get_health(){return health;}
  	public	int get_money(){return money;}
    public  int get_attack(){return attack;}
  	public	void add_key(Key new_key)
  	{
  		if(new_key.getColor() == 0) //blue  key
  			this.blue_key_count++;
  		else if(new_key.getColor() == 1) //red key
  			this.red_key_count++;
  		else if(new_key.getColor() == 2) //yellow key
  			this.yellow_key_count++;
  	}	
  
    public String[] get_status()
    {

      String[] digit = new String[8];
      int i,j;
      String str ;
      
      str = Integer.toString(floor);
      while(str.length()<2)
        str = "0"+str;
      str = "  "+str;

      digit[0] = str;
      
      str = Integer.toString(health);
      while(str.length()<4)
        str = "0"+ str;

      digit[1] = str;

      str = Integer.toString(attack);
      while(str.length()<3)
        str = "0" + str;
       
        str= " "+str; 
        digit[2] = str;      

      str = Integer.toString(defense);
      while(str.length()<3)
        str = "0" +str;
      str =" "+str;
      digit[3] = str;

      str = Integer.toString(money);
      while(str.length()<4)
        str = "0" + str;

      digit[4] = str;
      

      str = Integer.toString(yellow_key_count);

      while(str.length()<2)
        str = "0"+str;
      
      str = "  "+str;
      digit[5] = str;

      str = Integer.toString(blue_key_count);
      while(str.length()<2)
        str = "0"+str;

      str = "  "+str;
      digit[7] = str;

      str = Integer.toString(red_key_count);
      while(str.length()<2)
        str = "0"+str;

      str = "  "+str;
      digit[6] = str;
      return digit;

    }
  /*	
  	public void keyTyped(KeyEvent e) {}
    public void keyReleased(KeyEvent e){}
  	public void keyPressed(KeyEvent e)
	{
	 	switch(e.getKeyCode())
	 	{
            case KeyEvent.VK_UP	:
            	move(position_x-1,position_y);    
            	break;
            case KeyEvent.VK_DOWN :
                move(position_x+1,position_y); 
                break;  
			case KeyEvent.VK_LEFT :
                move(position_x,position_y-1);  
                break;
            case KeyEvent.VK_RIGHT :
                move(position_x,position_y+1);   
         		break;
      		case KeyEvent.VK_ENTER :
				         		
         		break;
         	default:
        }
	}
*/

	public	boolean act(Object item)
	{
		if(item instanceof Tile)
  		{
  			Tile item1 = (Tile) item;
  			if(!item1.get_enable())
  				return	false;
  		}
  		else if(item instanceof Stair)
  		{
  			Stair item1 = (Stair) item;
  			if(!item1.get_direction())
  			{	
  				gameMap[floor-1].set_stringmap(position_x,position_y," ");
  				floor--;
				floor_change=-2;
  			}
  			else
  			{	
  				gameMap[floor-1].set_stringmap(position_x,position_y," ");
  				floor++;
				floor_change=2;
			}
			//System.out.printf("%d %d %d\n", floor, position_x,position_y );
  			//gameMap[floor].set_stringmap(position_x,position_y,"@");
  			return	true;
  		}
  		else if(item instanceof	Door)
  		{
  			Door item1 = (Door)item;
  			int color = item1.getColor();
  			if(color == 0 )
  			{
  				if(red_key_count>0)
  					red_key_count--;
  				else
  					return	false;	
  			}
  			else if(color == 1)
  			{
  				if(blue_key_count>0)
  					blue_key_count--;
  				else
  					return	false;
  			}
  			else if(color==2)
  			{
  				if(yellow_key_count>0)
  					yellow_key_count--;
  				else
  					return	false;
  			}
			else return false;
  		}
  		else if(item instanceof Key)
  		{
  			Key item1 = (Key)item;
  			if(item1.getColor()==0)
  				red_key_count++;
  			else if(item1.getColor()==1)
  				blue_key_count++;
  			else if(item1.getColor()==2)
  				yellow_key_count++;
  		
  		}	
  		else if(item instanceof Monster)
  		{
  			Monster item1 = (Monster)item;
  			if(!fight(item1))
  			{	
  				return false;
  			}
			if (item1.get_attack()==150){
				gameMap[9].set_stringmap(4,3," ");
				gameMap[9].put_instance(3,4,(new Tile(true,position_x,position_y,' ')));
				gameMap[9].set_stringmap(5,3," ");
				gameMap[9].put_instance(3,5,(new Tile(true,position_x,position_y,' ')));
				gameMap[9].set_stringmap(6,3," ");
				gameMap[9].put_instance(3,6,(new Tile(true,position_x,position_y,' ')));
			}
  		}
  		else if(item instanceof Buff)
  		{
  			Buff item1 = (Buff)item;
  			int type = item1.get_type();
  			int value = item1.get_value();

  			if(type == 0)
  				set_health(health+value);
  			else if(type == 1)	 
  				set_attack(attack+value);
  			else if(type == 2)
  				set_defense(defense+value);
  		}
      else if(item instanceof Oldman)
      {
        Oldman item1 = (Oldman)item;
        ppl = (Item)item;
	talk = true;
        //item1.conversation(floor);
        return false;
      }
      else if(item instanceof Fairy)
      {
        talk = true;
        ppl = (Item)item;
        Fairy item1 = (Fairy)item;
        //item1.conversation(this);
        return false;
      } 
      else if(item instanceof Store)
      {
        altar = true;
        ppl = (Item)item;
        Store s = (Store) item;
        s.open(floor/5);
        return false;
      }
      else if(item instanceof Merchant)
      {
        altar = true;
        ppl = (Item)item;
        Merchant s = (Merchant) item;
        s.open(floor/5);
        return false;
      }
      else
  			return false;	
		
		return true;
	}

  	public int move(int new_x,int new_y)
  	{
  		//int x = new_x;
  		//int y = new_y;
  		Object item = new Object();
		boolean talk_ = talk;
  		if(new_x>=0 && new_x<11 && new_y>=0 && new_y<11)
  			 item = gameMap[floor-1].get_instance(new_x,new_y);
  		
  		//gameMap[floor-1].put_instance(position_x,position_y,this);

  		if(act(item))
  		{
			if (floor_change==1){
				gameMap[floor-1].put_instance(position_y,position_x,(new Stair(false,position_x,position_y,'l')));
				gameMap[floor-1].set_stringmap(position_x,position_y,"l");
				floor_change=0;
			}else if (floor_change==-1){
				gameMap[floor-1].put_instance(position_y,position_x,(new Stair(true,position_x,position_y,'k')));
				gameMap[floor-1].set_stringmap(position_x,position_y,"k");
				floor_change=0;
			}else{
				floor_change/=2;
				gameMap[floor-1-floor_change].put_instance(position_y,position_x,(new Tile(true,position_x,position_y,' ')));
				gameMap[floor-1-floor_change].set_stringmap(position_x,position_y," ");
			}
			//System.out.printf("floor-1 = %d\n",floor-1);
  			gameMap[floor-1].put_instance(new_y,new_x,this);
  			gameMap[floor-1].set_stringmap(new_x,new_y,"@");
			//gameMap[floor-1].put_instance(new_y,new_x,(new Tile(true)));
			if (position_x<new_x)
				gui_move(32, 0);
			else if (position_x>new_x)
				gui_move(-32, 0);
			else if(position_y<new_y)
				gui_move(0, 32);			
			else if (position_y>new_y)
				gui_move(0, -32);
  			position_x = new_x;
  			position_y = new_y;
			System.out.printf("hp:%d atk:%d def:%d Gold:%d\n", health, attack, defense, money);
			System.out.println("=======================================");
			if(item instanceof Monster){
				Monster item1 = (Monster)item;
				return item1.get_num();
			}
			return 0;
  		}
  		//mod
  		else if(item instanceof Monster)
  		{
  		  //System.out.println("kill by mon 2");
  		    return -4;
  		}
		else if(item instanceof Fairy)
		{
			if(!talk_){
				gameMap[floor-1].set_stringmap(new_x,new_y," ");
				gameMap[floor-1].put_instance(new_y,new_x,(new Tile(true,new_x,new_y,' ')));
				return 0;
			}
			return -2;
		}
		else if(item instanceof Oldman)
		{
			if(!talk_){
				gameMap[floor-1].set_stringmap(new_x,new_y," ");
				gameMap[floor-1].put_instance(new_y,new_x,(new Tile(true,new_x,new_y,' ')));
				return 0;
			}
			return -3;
		}		
		return -1;
  	}
	
	public void gui_move(int x, int y) {
		int nx = this.x() + x;
		int ny = this.y() + y;
		this.setX(nx);
		this.setY(ny);
	}

  	public	boolean fight(Monster monster)
  	{
  		int damage = attack - monster.get_defense();
		if (damage<0)
			return false;
		Skill monster_skill = monster.get_skill();

  		if(monster_skill instanceof Rush)
  			monster.fight(this);

  		while(monster.get_health()>0 && this.health>0)
  		{
  			monster.set_health(monster.get_health()-damage);
  			if(monster.get_health()<=0)
  				break;
  			monster.fight(this);
  		}	
  		
  		if(health<=0)
  			return	false;
  		else
  		{
  			set_money(monster.get_money()+money);
  			return true;
  		}
  	}

  	private void fail()
  	{

  	}

  	private void complete_mission()
  	{

  	}
}