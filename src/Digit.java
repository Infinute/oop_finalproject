
public class Digit extends Item {
    
    public Digit (int x, int y, char c) {
        super(x, y, c);
    }
    
    public void update (char c) {
        setImg(c);
    }
    
}
