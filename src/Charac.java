
public class Charac extends Item {
    
    /*
     * A-S: 19 monsters, 
     * T: fairy, U:old man, V: merchant, W: altar(1*3)
    */
    public Charac (int x, int y, char c) {
        super(x, y, c);
    }

    public void kill () {
        super.use();
    }
}
